package com.ds.onlinemedication.service;

import com.ds.onlinemedication.dto.ActivityBuilder;
import com.ds.onlinemedication.dto.ActivityDTO;
import com.ds.onlinemedication.repositories.ActivityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ActivityService implements Serializable {

    private ActivityRepository activityRepository;

    @Autowired
    public ActivityService(ActivityRepository activityRepository) {
        this.activityRepository = activityRepository;
    }

    public ActivityService() {
    }

    public List<ActivityDTO> findAll() {
        return activityRepository.findAll()
                .stream()
                .map(ActivityBuilder::generateActivityDTO)
                .collect(Collectors.toList());
    }

}
