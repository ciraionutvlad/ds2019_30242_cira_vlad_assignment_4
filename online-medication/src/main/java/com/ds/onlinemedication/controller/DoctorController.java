package com.ds.onlinemedication.controller;

import com.ds.onlinemedication.dto.ActivityDTO;
import com.ds.onlinemedication.dto.DoctorDTO;
import com.ds.onlinemedication.model.ChartPoint;
import com.ds.onlinemedication.service.DoctorService;
import com.ds.onlinemedication.soap.SOAPConnector;
import com.ds.onlinemedication.xml.hospital.ActivityDetailsRequest;
import com.ds.onlinemedication.xml.hospital.ActivityDetailsResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Controller
@RequestMapping(value = "/")
public class DoctorController {

    private DoctorService doctorService;
    private SOAPConnector soapConnector;

    @Autowired
    public DoctorController(DoctorService doctorService, SOAPConnector soapConnector) {
        this.doctorService = doctorService;
        this.soapConnector = soapConnector;
    }

    @GetMapping("add-doctor")
    public String showAddDoctor(DoctorDTO doctorDTO) {
        return "add-doctor";
    }

    @GetMapping("doctor/edit/{id}")
    public String showUpdateForm(@PathVariable("id") int id, Model model) {
        DoctorDTO doctorDTO = doctorService.findById(id);
        model.addAttribute("doctor", doctorDTO);
        return "update-doctor";
    }

    @GetMapping(value = "doctor/{id}")
    public DoctorDTO findById(@PathVariable("id") Integer id) {
        return doctorService.findById(id);
    }

    @GetMapping(value = "doctor/all")
    public String findAll(Model model) {
        List<DoctorDTO> list = doctorService.findAll();
        model.addAttribute("doctors", list);
        return "list-doctor";
    }

    @PostMapping(value = "doctor/add")
    public String insert(@Valid DoctorDTO doctorDTO, Model model) {
        doctorService.insert(doctorDTO);
        model.addAttribute("doctors", doctorService.findAll());
        return "list-doctor";
    }

    @PostMapping(value = "doctor/update/{id}")
    public String update(@PathVariable("id") int id, @Valid DoctorDTO doctorDTO, Model model) {
        doctorDTO.setId(id);
        doctorService.update(doctorDTO);
        model.addAttribute("doctors", doctorService.findAll());
        return "list-doctor";
    }

    @GetMapping(value = "doctor/delete/{id}")
    public String delete(@PathVariable("id") Integer id, Model model) {
        doctorService.delete(id);
        model.addAttribute("doctors", doctorService.findAll());
        return "list-doctor";
    }

    @GetMapping("view-history")
    public String viewHistory(@RequestParam(value = "id") String id, Model model) {
        ActivityDetailsRequest request = new ActivityDetailsRequest();
        request.setPatient(Integer.parseInt(id));

        ActivityDetailsResponse response = (ActivityDetailsResponse) soapConnector.callWebService("http://localhost:8080/service/activity-details", request);
        showChart(response, model);

        return "chart";
    }

    private void showChart(ActivityDetailsResponse response, Model model) {

        List<ChartPoint> chartPoints = new ArrayList<>();
        List<ActivityDTO> list = response.getActivity().stream().map(o -> (ActivityDTO) o).collect(Collectors.toList());

        for (ActivityDTO activity : list) {
            ChartPoint chartPoint = new ChartPoint();
            chartPoint.setLabel(activity.getActivity());

            chartPoint.setY(TimeUnit.MILLISECONDS.toSeconds(activity.getEnd()) - TimeUnit.MILLISECONDS.toSeconds(activity.getStart()));

            chartPoints.add(chartPoint);
        }

        model.addAttribute("points", chartPoints);
    }
}
