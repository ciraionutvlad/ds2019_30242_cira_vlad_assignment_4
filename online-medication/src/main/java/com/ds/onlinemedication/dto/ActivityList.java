package com.ds.onlinemedication.dto;

import java.util.ArrayList;
import java.util.List;

//@XmlRootElement
public class ActivityList {

    private List<ActivityDTO> list;

    public ActivityList() {
    }

    //@XmlElement
    public List<ActivityDTO> getList() {
        System.out.println("getList " + hashCode());
        return list;
    }

    public void setList(List<ActivityDTO> list) {
        this.list = new ArrayList<>();
        this.list = list;
    }
}
