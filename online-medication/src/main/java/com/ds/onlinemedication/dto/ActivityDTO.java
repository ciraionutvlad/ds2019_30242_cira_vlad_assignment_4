package com.ds.onlinemedication.dto;

import com.ds.onlinemedication.entities.Patient;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Activity", propOrder = {
        "id",
        "activity",
        "start",
        "end"
})
public class ActivityDTO implements Serializable {

    @XmlTransient
    private static final String PATTERN = "yyyy-MM-dd HH:mm:ss";
    @XmlTransient
    private SimpleDateFormat sdf = new SimpleDateFormat(PATTERN);

    @XmlElement(required = true)
    private int id;
    @XmlElement(required = true)
    private String activity;
    @XmlElement(required = true)
    private String start;
    @XmlElement(required = true)
    private String end;
    @XmlTransient
    private Patient patientId;

    public ActivityDTO() {
    }

    public ActivityDTO(int id, String activity, String start, String end, Patient patientId) {
        this.id = id;
        this.activity = activity;
        this.start = start;
        this.end = end;
        this.patientId = patientId;
    }

    public ActivityDTO(int id, String activity, String start, String end) {
        this.id = id;
        this.activity = activity;
        this.start = start;
        this.end = end;
    }

    public int getId() {
        return id;
    }

    public String getActivity() {
        return activity;
    }

    public long getStart() {
        long result = 0;
        try {
            Date startDate = sdf.parse(start);
            result = startDate.getTime();

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return result;
    }

    public long getEnd() {
        long result = 0;
        try {
            Date startDate = sdf.parse(end);
            result = startDate.getTime();

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return result;
    }

    public Patient getPatientId() {
        return patientId;
    }

    @Override
    public String toString() {
        return "ActivityDTO{" +
                "id=" + id +
                ", activity='" + activity + '\'' +
                ", startMillis='" + start + '\'' +
                ", endMillis='" + end + '\'' +
                '}';
    }
}
