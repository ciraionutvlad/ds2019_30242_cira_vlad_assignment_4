package com.ds.onlinemedication.dto;

import com.ds.onlinemedication.entities.Activity;

public class ActivityBuilder {

    public static ActivityDTO generateActivityDTO(Activity activity) {
        return new ActivityDTO(activity.getId(), activity.getActivity(), activity.getStart(),
                activity.getEnd(), activity.getPatient());
    }
}
