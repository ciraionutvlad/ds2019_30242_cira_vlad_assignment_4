package com.ds.onlinemedication.repositories;

import com.ds.onlinemedication.entities.Activity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ActivityRepository extends JpaRepository<Activity, Integer> {

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("SELECT new Activity(a.id, a.activity, a.start, a.end) FROM Activity a WHERE a.patient.id = :patientId")
    List<Activity> findAllById(@Param("patientId") int patientId);
}
