package com.ds.onlinemedication.repositories;

import com.ds.onlinemedication.entities.Caregiver;
import com.ds.onlinemedication.entities.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface PatientRepository extends JpaRepository<Patient, Integer> {

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("UPDATE Patient d SET d.name = :name, d.birthDate = :birthDate, d.gender = :gender, " +
            "d.address = :address ,d.medicalRecord = :medicalRecord, d.caregiver = :caregiver WHERE d.id = :id")
    int updatePatient(@Param("id") int id,
                      @Param("name") String name,
                      @Param("birthDate") String birthDate,
                      @Param("gender") String gender,
                      @Param("address") String address,
                      @Param("medicalRecord") String medicalRecord,
                      @Param("caregiver") Caregiver caregiver);
}
