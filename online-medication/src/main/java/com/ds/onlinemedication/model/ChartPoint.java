package com.ds.onlinemedication.model;

public class ChartPoint {
    private String label;
    private Long Y;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Long getY() {
        return Y;
    }

    public void setY(Long y) {
        Y = y;
    }
}
