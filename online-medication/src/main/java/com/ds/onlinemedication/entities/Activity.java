package com.ds.onlinemedication.entities;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.io.Serializable;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "activity")
public class Activity implements Serializable {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "activity")
    @JsonProperty("activity")
    private String activity;

    @Column(name = "start")
    @JsonProperty("start")
    private String start;

    @Column(name = "end")
    @JsonProperty("end")
    private String end;

    @ManyToOne//(fetch = FetchType.LAZY)
    //JsonProperty("patient_id")
    private Patient patient;


    public Activity() {
    }

    public Activity(Integer id, String activity, String start, String end, Patient patient) {
        this.id = id;
        this.activity = activity;
        this.start = start;
        this.end = end;
        this.patient = patient;
    }

    public Activity(Integer id, String activity, String start, String end) {
        this.id = id;
        this.activity = activity;
        this.start = start;
        this.end = end;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    @Override
    public String toString() {
        return "Activity{" +
                "patient='" + patient + '\'' +
                ", activity='" + activity + '\'' +
                ", startMillis=" + start +
                ", endMillis=" + end +
                '}';
    }
}
