package com.ds.onlinemedication.soap;

import com.ds.onlinemedication.dto.ActivityBuilder;
import com.ds.onlinemedication.entities.Activity;
import com.ds.onlinemedication.repositories.ActivityRepository;
import com.ds.onlinemedication.xml.hospital.ActivityDetailsRequest;
import com.ds.onlinemedication.xml.hospital.ActivityDetailsResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.util.List;
import java.util.stream.Collectors;

@Transactional
@Endpoint
public class ActivityEndpoint {
    private static final String NAMESPACE_URI = "http://www.onlinemedication.com/service/hospital";

    private ActivityRepository activityRepository;

    @Autowired
    public ActivityEndpoint(ActivityRepository ActivityRepository) {
        this.activityRepository = ActivityRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "ActivityDetailsRequest")
    @ResponsePayload
    public ActivityDetailsResponse getActivity(@RequestPayload ActivityDetailsRequest request) {
        List<Activity> activity = activityRepository.findAllById(request.getPatient());

        ActivityDetailsResponse response = new ActivityDetailsResponse();
        response.setActivity(activity.stream()
                .map(ActivityBuilder::generateActivityDTO)
                .collect(Collectors.toList()));

        return response;
    }
}